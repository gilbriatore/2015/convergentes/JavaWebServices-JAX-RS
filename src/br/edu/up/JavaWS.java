package br.edu.up;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.security.ProtectionDomain;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/javaws")
public class JavaWS {
	
	private static String getPath() {
		ProtectionDomain d = JavaWS.class.getProtectionDomain();
		return d.getCodeSource().getLocation().getPath();
	}
	
	private URI getUri() {
		UriBuilder builder = UriBuilder.fromUri("http://localhost:9090/javaws");
		return builder.build(); 
	}

    //http://localhost:9090/javaws	
	@GET
	@Produces("text/html")
	public String getHTML() {
		return "<h1>Texto em HTML</h1>";
	}

	//http://localhost:9090/javaws/{codigo}	
	@GET
	@Path("/{codigo}")
	@Produces("text/html;charset=UTF8")
	public String getHTML(@PathParam("codigo") String codigo) {

		switch (codigo) {
		case "1":
			return "<h1>Texto HTML c�digo 1!</h1>";
		case "2":
			return "<h1>Texto HTML c�digo 2!</h1>";
		}
		return "<h1>Texto HTML padr�o!</h1>";
	}

	//http://localhost:9090/javaws/composto	
	@GET
	@Path("/composto")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.TEXT_HTML, 
		        MediaType.TEXT_XML, MediaType.APPLICATION_JSON })
	public String get(@HeaderParam("accept") String accept) {
		switch (accept) {
		case MediaType.TEXT_PLAIN:
			return "Texto Puro";
		case MediaType.TEXT_HTML:
			return "<h1>Texto HTML</h1>";
		case MediaType.TEXT_XML:
			return "<texto>Texto XML</texto>";
		}
		return "{\"texto\" : \"Texto JSON\"}";
	}

	//http://localhost:9090/javaws/imagem	
	@GET
	@Path("/imagem")
	@Produces({"image/png", "image/jpeg"})
	public Response getImagem() {

		File arquivo = new File(getPath() + "/obama.jpg");
		
		ResponseBuilder response = Response.ok((Object) arquivo);
		response.header("Content-Disposition", "attachment; " + arquivo.getName());
		return response.build();
	}
	
	//http://localhost:9090/javaws/arquivo
	@GET
	@Path("/arquivo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getArquivo() {

		File arquivo = new File(getPath() + "/arquivo.pdf");

		ResponseBuilder response = Response.ok(arquivo, MediaType.APPLICATION_OCTET_STREAM);
		response.header("Content-Disposition", "attachment; filename=\"" + arquivo.getName());
		return response.build();
	}

	//http://localhost:9090/javaws/imagem
	@POST
	@Path("/imagem")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadImagem(@FormDataParam("imagem") InputStream is) {
		 
		try {	
			
			File arquivoDestino = new File("d:/exemplo.jpg");
			FileOutputStream fos = new FileOutputStream(arquivoDestino);
			
			int qtdeDeBtyes;
			byte[] buffer = new byte[2048];
			while ((qtdeDeBtyes = is.read(buffer)) != -1) {
				fos.write(buffer, 0, qtdeDeBtyes);
			}
			fos.flush();
			fos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		return Response.created(getUri()).build();
	}
}